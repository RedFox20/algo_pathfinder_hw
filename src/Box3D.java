import javafx.scene.shape.Box;

public class Box3D extends Box
{
    public final Position3D pos = new Position3D(this);

    public Box3D(float sizeX, float sizeY, float sizeZ) { super(sizeX, sizeZ, sizeY); }
}
