import java.util.Locale;

/**
 * @brief Astar node representing its position AND state in the graph
 * A list of links is precomputed for faster traversal
 */
public class AstarNode
{
    public float FScore = 0; // F = H + G(current score from src to dst)
    public float HScore = 0; // heuristic score: XY.manhattanDistance + deltaHeight*10
    public float GScore = 0; // accumulated sum of H values, giving total distance
    public int   OpenID = 0; // optimization: used to check if we've opened this node or not

    // A closed node is one that has been evaluated by A* and should be ignored in further checks
    public boolean Closed = false;

    public AstarNode Prev    = null; // previous node in the open path
    public int NumLinks      = 0;
    public AstarLink[] Links = new AstarLink[8];

    // X,Y,Z position in local terrain coords for heuristic preinit
    public float X; // RIGHT
    public float Y; // FWD
    public float Z; // UP

    /** @brief Initializes AstarNode with default valus and XYZ position */
    AstarNode(float x, float y, float z) { X=x; Y=y; Z=z; }

    /** return A delta XYZ vector between this and node b */
    public final Vector3 deltaXYZ(AstarNode b) { return new Vector3(X-b.X, Y-b.Y, Z-b.Z); }

    @Override public String toString() {
        return String.format(Locale.US, "{%.1f,%.1f,%.1f}", X, Y, Z);
    }
}

