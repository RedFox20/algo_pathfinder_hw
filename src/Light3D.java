import javafx.scene.PointLight;
import javafx.scene.paint.Color;

public class Light3D extends PointLight
{
    public final Position3D pos = new Position3D(this);

    public Light3D(Color lightColor) { super(lightColor); }
}
