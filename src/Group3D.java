import javafx.scene.Group;
import javafx.scene.Node;

public class Group3D extends Group
{
    public final Position3D pos = new Position3D(this);

    public Group3D() {}
    public Group3D(Node... children) { super(children); }
}
