import javafx.scene.PerspectiveCamera;

public class Camera3D extends PerspectiveCamera
{
    public final Position3D pos = new Position3D(this);

    public Camera3D() { super(true); }
}
