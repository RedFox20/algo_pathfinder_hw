
/**
 * @brief Height method controls how height component is weighed
 */
public enum HeightMethod // controls how height component is weighed
{
    Linear,      // height method uses same method as DistanceMethod
    Multiply,    // height value is multiplied with a constant value
    Exponential, // height value is raised to an exponent
}
