import com.sun.javafx.geom.Vec3f;
import javafx.geometry.Point3D;

import java.util.Locale;

/**
 * An utility LHS Vector3 class.
 * This is mainly used to keep the algorithms portable between renderers.
 *
 * A note on JavaFX 3D coordinate system. It doesn't use LHS or RHS, but a custom system
 * where Y is down and Z points away from camera, most likely due to JFX's 2D legacy.
 * X-right
 * Y-down
 * Z-forward
 * We use LHS coords internally and convert them to JFX format on demand.
 */
public class Vector3
{
    public float x, y, z;

    public Vector3()                          { x = 0.f;  y = 0.f;  z = 0.f;  }
    public Vector3(Vector3 v)                 { x = v.x;  y = v.y;  z = v.z;  }
    public Vector3(float x, float y, float z) { this.x=x; this.y=y; this.z=z; }

    // convert from JFX coordinates to LHS:
    public Vector3(Point3D jfxPoint) {
        x = (float)jfxPoint.getX();
        y = (float)jfxPoint.getZ();
        z = -(float)jfxPoint.getY();
    }

    // JFX point3d from this LHS vector3
    public final Point3D toJFXPoint() { return new Point3D(x, -z, y); }

    public final Vector3 negate() { return new Vector3(-x, -y, -z); }
    public final boolean isZero() { return x == 0.0f && y == 0.0f && z == 0.0f; }
    public final void set(float X, float Y, float Z) { x=X; y=Y; z=Z; }
    @Override public String toString() { return String.format(Locale.US, "{%.2f,%.2f,%.2f}", x, y, z); }

    public final Vector3 add(float X, float Y, float Z) { return new Vector3(x+X,y+Y,z+Z); }
    public final Vector3 sub(float X, float Y, float Z) { return new Vector3(x-X,y-Y,z-Z); }
    public final Vector3 mul(float X, float Y, float Z) { return new Vector3(x*X,y*Y,z*Z); }
    public final Vector3 div(float X, float Y, float Z) { return new Vector3(x/X,y/Y,z/Z); }

    public final Vector3 add(Vector3 v) { return new Vector3(x+v.x,y+v.y,z+v.z); }
    public final Vector3 sub(Vector3 v) { return new Vector3(x-v.x,y-v.y,z-v.z); }
    public final Vector3 mul(Vector3 v) { return new Vector3(x*v.x,y*v.y,z*v.z); }
    public final Vector3 div(Vector3 v) { return new Vector3(x/v.x,y/v.y,z/v.z); }

    public final Vector3 add(float f) { return new Vector3(x+f,y+f,z+f); }
    public final Vector3 sub(float f) { return new Vector3(x-f,y-f,z-f); }
    public final Vector3 mul(float f) { return new Vector3(x*f,y*f,z*f); }
    public final Vector3 div(float f) { return new Vector3(x/f,y/f,z/f); }

    public final float length() { return (float)Math.sqrt(x*x+y*y+z*z); }
    public final float sqlen()  { return x*x+y*y+z*z; }

    /** @return A normalized COPY of this vector */
    public final Vector3 normalized() { return new Vector3(this).normalize(); }

    /** @brief Normalizes THIS vector and returns reference to self */
    public final Vector3 normalize() {
        float len = length();
        if (len != 0.0f) {
            x /= len;
            y /= len;
            z /= len;
        }
        return this;
    }

    // creates a right XY normal vector between two points
    public final Vector3 rightNormalXY(Vector3 b) {
        float nx = b.x - x;
        float ny = b.y - y;
        return new Vector3(-ny, nx, 0).normalize();
    }
}
