
/**
 * @brief Represents an Astar [A]-->[B] node link with its traversal cost
 */
public class AstarLink
{
    public AstarNode Node; // endpoint(B) of the link
    public float Cost;     // precalculated cost of traversing this link

    // does not initialize cost
    public AstarLink(AstarNode node)
    {
        Node = node;
    }

    public AstarLink(AstarNode node, float cost)
    {
        Node = node;
        Cost = cost;
    }
}