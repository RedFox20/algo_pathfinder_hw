import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;
import java.util.ArrayDeque;
import java.util.ArrayList;

/**
 * A 3D graph representing the entire heightmap
 * Also provides an interface for pathfinding
 */
public class Astar
{
    private final static float GridSize   = 5.0f;    // distance between grid points
    private final static float GridHeight = 100.0f;  // maximum grid height

    public final static float MaxHeightMultiplier = 50.0f;
    public final static float MaxHeightExponent   = 8.0f;

    private static float HeightMultiplier = 5.0f; // height multiplier for HeightMethod Multiply
    private static float HeightExponent   = 1.0f;  // height exponent for HeightMethod Exponential
    private HeightMethod   HMethod = HeightMethod.Multiply;
    private DistanceMethod DMethod = DistanceMethod.Accurate;

    public AstarNode[] Grid;
    public int Width;        // size in grid indices
    public int Height;

    // LHS size in world coordinates +X=right +Y=forward +Z=up
    public Vector3 WorldSize = new Vector3();
    public Image Texture;

    // pathfinder specific
    public  int NumOpened   = 0; // statistic, number of Nodes opened
    public  int NumReopened = 0; // statistic, number of Nodes reopened for more accurate path
    public  int MaxDepth    = 0; // statistic, maximum achieved depth of OpenList
    private int OpenID      = 0; // pathfinder session ID
    private AstarNodeVector OpenList = new AstarNodeVector();

    /**
     * @brief Initializes entire graph from a source png map
     * @note The map path is hardcoded for testing purposes!
     */
    public final void loadMap()
    {
        Texture = new Image("http://enos.itcollege.ee/~jrebane/algo/testmap.png");
        PixelReader pr = Texture.getPixelReader();
        Width  = (int)Texture.getWidth();
        Height = (int)Texture.getHeight();
        WorldSize.x = Width  * GridSize;
        WorldSize.y = Height * GridSize;
        WorldSize.z = 0;
        OpenList = new AstarNodeVector((Width * Height) / 4); // prealloc OpenList

        int count = Width * Height;
        Grid = new AstarNode[count];

        for (int y = 0; y < Height; ++y)
            for (int x = 0; x < Width; ++x)
            {
                Color c = pr.getColor(x, Height-y-1); // fix Y coordinate (OpenGL texture order?)
                float z = (float)c.getBrightness() * GridHeight;
                if (z > WorldSize.z)
                    WorldSize.z = z;

                int i = (y * Width) + x;
                Grid[i] = new AstarNode(x*GridSize, y*GridSize, z);
            }

        // preinitialize grid links, this is an iterative floodfill
        ArrayDeque<AstarNode> open = new ArrayDeque<>();
        for (int i = 0; i < count; ++i)
        {
            AstarNode node = Grid[i];
            while (true)
            {
                if (node.NumLinks == 0) // is the node uninitialized?
                {
                    add(open, node,  0, +1);  // N
                    add(open, node, +1, +1); // NE
                    add(open, node, +1,  0);  // E
                    add(open, node, +1, -1); // SE
                    add(open, node,  0, -1);  // S
                    add(open, node, -1, -1); // SW
                    add(open, node, -1,  0);  // W
                    add(open, node, -1, +1); // NW
                }
                if (open.isEmpty())
                    break;
                node = open.pop();
            }
        }

        updateNodeWeights(); // and finally; precalculate node weights
    }

    /**
     * Tries to add a LINK to this node
     * @param open Openlist for floodfill bookmarking
     * @param node Node to add link to
     * @param offX OffsetX where to try and get the link node
     * @param offY OffsetY where to try and get the link node
     */
    private void add(ArrayDeque<AstarNode> open, AstarNode node, int offX, int offY)
    {
        int x = (int)(node.X / GridSize) + offX; // convert world XY to grid index XY
        int y = (int)(node.Y / GridSize) + offY;
        AstarNode target = getNode(x, y);
        if (target == null)
            return; // don't add out of bounds items

        node.Links[node.NumLinks++] = new AstarLink(target);
        if (target.NumLinks == 0)
            open.add(target);
    }

    private AstarNode getNode(int x, int y)
    {
        if (x < 0 || Width <= x || y < 0 || Height <= y)
            return null;
        return Grid[y * Width + x];
    }

    /** @note This method is slow. Number of iterations is ~ Width*Height*8 */
    private void updateNodeWeights()
    {
        for (AstarNode n : Grid) {
            final int numLinks = n.NumLinks;
            for (int i = 0; i < numLinks; ++i) {
                final AstarLink link = n.Links[i];
                link.Cost = heuristicDistance(n, link.Node);
            }
        }
    }

    public final boolean distanceMethod(DistanceMethod method) {
        if (method == DMethod) return false;
        DMethod = method;
        updateNodeWeights();
        return true;
    }
    public final boolean heightMethod(HeightMethod method) {
        if (method == HMethod) return false;
        HMethod = method;
        updateNodeWeights();
        return true;
    }
    public final boolean heightMultiplier(float multiplierValue) {
        if (multiplierValue == HeightMultiplier) return false;
        HeightMultiplier = multiplierValue;
        updateNodeWeights();
        return true;
    }
    public final boolean heightExponent(float exponentValue) {
        if (exponentValue == HeightExponent) return false;
        HeightExponent = exponentValue;
        updateNodeWeights();
        return true;
    }
    public final DistanceMethod distanceMethod() { return DMethod; }
    public final HeightMethod   heightMethod()   { return HMethod; }
    public final float heightMultiplier() { return HeightMultiplier; }
    public final float heightExponent()   { return HeightExponent;   }



    /** @return Appropriate distance value depending on the method */
    private static float getDistance(DistanceMethod dm, float dx, float dy, float dz)
    {
        switch (dm) {default:
            case ManhattanDistance: return Math.abs(dx) + Math.abs(dy) + Math.abs(dz); // classic manhattan XYZ
            case SquaredDistance:   return dx*dx + dy*dy + dz*dz;                      // square distance -- very inprecise, but cool to test
            case Accurate:          return (float)Math.sqrt(dx*dx + dy*dy + dz*dz);    // accurate distance
        }
    }

    /** @brief calculate slope for our heuristic model */
    private static float calculateSlope(HeightMethod hm, final float distance, final float deltaHeight)
    {
        // 2^(x*5) from 0 to 1
        // http://www.wolframalpha.com/input/?i=((2)%5E(x*5))+from+0+to+1
        float x = Math.abs(deltaHeight) / distance;
        float y = (float)Math.pow(2.0, x*GridSize);
        if (deltaHeight > 0.0f)
            y *= 1.5f; // going uphill is harder than going downhill

        switch (hm) {default:
            case Linear:      return y;
            case Multiply:    return y*HeightMultiplier;
            case Exponential: return (float)Math.pow(y, HeightExponent);
        }
    }

    // need to give additional penalty for travelling parallel to steep slopes
    private float calculatePerpendicularSlope(final float abDistance, AstarNode a, AstarNode b)
    {
        if (abDistance > 2*GridSize)
            return 0.0f; // dont give penalty if measuring long distance
        // delta XY
        final int ax = getGridX(a), ay = getGridY(a);
        final int bx = getGridX(b), by = getGridY(b);
        int nx = bx-ax, ny = ay-by; // right perpendicular XY

        AstarNode other = getNode(ax+nx, ay+ny);
        if (other == null) other = getNode(ax-nx, ax-ny); // left perpendicular XY
        if (other == null) other = getNode(bx+nx, bx+ny);
        if (other == null) other = getNode(bx-nx, bx-ny);

        final Vector3 dv = a.deltaXYZ(other);
        final float distance = getDistance(DMethod, dv.x, dv.y, dv.z);
        final float slope = calculateSlope(HMethod, distance, dv.z);
        return slope * 0.5f; // perpendicular slope has less priority
    }

    private static int SlopeOutputCounter = 0;

    /**
     * Full heuristic distance value used in this Astar algorithm. Parameterized to allow runtime tweaking.
     * @param a Start node
     * @param b End node
     * @return Heuristic value for Astar
     */
    private float heuristicDistance(AstarNode a, AstarNode b)
    {
        final Vector3 dv       = a.deltaXYZ(b); // direction: values can be negative
        final float   distance = getDistance(DMethod, dv.x, dv.y, dv.z);
        final float   slope    = calculateSlope(HMethod, distance, dv.z);
        final float   pslope   = calculatePerpendicularSlope(distance, a, b);

//        if (++SlopeOutputCounter >= 2500)
//        {
//            System.out.printf("z=%+.2f d=%.2f s=%.2f ps=%.2f\n", dv.z, distance, slope, pslope);
//            SlopeOutputCounter = 0;
//        }
        return distance + slope + pslope;
    }


    /** @brief Get X index in the graph -- for node lookup */
    private static int getGridX(AstarNode node) { return (int)(node.X / GridSize); }
    private static int getGridY(AstarNode node) { return (int)(node.Y / GridSize); }

    /** @brief Classic Manhattan XY distance to predict output list capacity */
    private static int manhattanDistanceXY(AstarNode a, AstarNode b)
    {
        return Math.abs(getGridX(a)-getGridY(b)) + Math.abs(getGridY(a)-getGridY(b));
    }

    /** @brief Finds a node corresponding to the given terrain position */
    private AstarNode findNode(Vector3 terrainPos)
    {
        int x = (int)(terrainPos.x / GridSize);
        int y = (int)(terrainPos.y / GridSize);
        if (x < 0 || Width <= x || y < 0 || Height <= y)
            return null;
        return Grid[y * Width + x];
    }

    /**
     * @brief This is the core Astar search routine
     * @param terrainStartPos Valid starting position in !! TERRAIN !! coords
     * @param terrainEndPos Valid end position in !! TERRAIN !! coords
     * @param explored If not null, then all explored links are output in pairs as:  [A,B,  B,C,  C,D]
     * @return Final path expressed as a node list: [A,B,C,D,E,F,...,Z]
     */
    public final ArrayList<Vector3> findPath(Vector3 terrainStartPos, Vector3 terrainEndPos,
                                             ArrayList<Vector3> explored)
    {
        NumOpened   = 0; // statistics
        NumReopened = 0;
        MaxDepth    = 0;

        ArrayList<Vector3> path = new ArrayList<>();
        final AstarNode start = findNode(terrainStartPos);
        final AstarNode end   = findNode(terrainEndPos);
        if (start == null || end == null)
            return path; // empty path

        path.ensureCapacity(manhattanDistanceXY(start, end));
        AstarNode head = start;
        head.Prev = null;
        final int  openID = ++OpenID; // get new ID to differ between unexplored and reopenable nodes

        while (head != end)
        {
            final AstarNode prev   = head.Prev;
            final float headGScore = head.GScore;
            final int     numLinks = head.NumLinks;

            for (int i = 0; i < numLinks; ++i)
            {
                final AstarLink link = head.Links[i];
                final AstarNode n    = link.Node;
                if (n == prev)
                    continue; // avoid circular references

                if (openID == n.OpenID) // we have opened this Node before; it's a reopen
                {
                    if (n.Closed)
                        continue; // don't touch if it's CLOSED

                    final float gscore = headGScore + link.Cost; // new gain score
                    if (gscore >= n.GScore)
                        continue; // if the new gain is worse, then don't touch it

                    n.FScore = n.HScore + gscore;
                    n.GScore = gscore;
                    n.Prev   = head;

                    /** @note: this was a reopen */
                    ++NumOpened;
                    ++NumReopened;
                    OpenList.repos(n); // reposition item (due to new FScore)
                }
                else // open this 'fresh' node for path consideration
                {
                    // calculate HScore
                    // (abs(distX) + abs(distY))*8
                    final float hscore = heuristicDistance(n, end);
                    final float gscore = headGScore + link.Cost;
                    n.HScore = hscore;
                    n.GScore = gscore;
                    n.FScore = hscore + gscore;
                    n.Closed = false;
                    n.Prev   = head;
                    n.OpenID = openID;

                    /** @note: first open: insert to openlist (sorted insert!)*/
                    ++NumOpened;
                    OpenList.insert(n);
                }

                final int size = OpenList.size();
                if (size > MaxDepth) MaxDepth = size; // statistic, max openlist depth

                if (explored != null) // output graph of all explored nodes
                {
                    explored.add(new Vector3(head.X,head.Y,head.Z));
                    explored.add(new Vector3(n.X,n.Y,n.Z));
                }
            }

            // after inserting into the sorted list we get the heuristically best node available
            if (OpenList.empty())
                break; // or not.

            head = OpenList.pop();
            head.Closed = true;
        }

        // finalization: build the out path
        head = end;
        do {
            path.add(new Vector3(head.X,head.Y,head.Z));
        } while ((head = head.Prev) != null &&
                head != start);/**@note bugfix for looping path*/

        OpenList.clear(); // reset openlist
        return path;
    }

    /**
     * Generates a new terrain mesh from the current 3D state of the graph
     * @return Displayable 3D MeshView
     */
    public final MeshView3D createTerrainMesh()
    {
        final int size = Grid.length;
        Vector3[] grid = new Vector3[size];
        for (int i = 0; i < size; ++i) {
            AstarNode n = Grid[i];
            grid[i] = new Vector3(n.X, n.Y, n.Z);
        }
        return MeshView3D.generateGridTerrain(grid, Width, Height, WorldSize);
    }
}
