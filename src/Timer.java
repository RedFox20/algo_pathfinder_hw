
/**
 * Timer utility for managing DeltaTime in the game loop
 */
public class Timer
{
    private long Start;
    public Timer()          { Start = System.nanoTime(); }
    public void   start()   { Start = System.nanoTime(); }
    public double elapsed() { return (System.nanoTime() - Start) / 1000000000.0; }
    public double next() {
        long now = System.nanoTime();
        double t = (now - Start) / 1000000000.0;
        Start = now;
        return t;
    }
}