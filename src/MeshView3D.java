import javafx.scene.shape.Mesh;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.TriangleMesh;
import javafx.scene.shape.VertexFormat;

import java.util.ArrayList;

/** Helper class for managing MeshViews */
public class MeshView3D extends MeshView
{
    public Position3D pos = new Position3D(this);

    public MeshView3D() {}
    public MeshView3D(Mesh mesh) { super(mesh); }

    /**
     * @brief Generates grid terrain from LHS coordinates
     * @note The output mesh is JFX specific
     */
    public static MeshView3D generateGridTerrain(
            Vector3[] grid, final int w, final int h, Vector3 worldSize)
    {
        TriangleMesh mesh = new TriangleMesh();
        // Vec3 pos;
        // Vec2 tex;
        mesh.setVertexFormat(VertexFormat.POINT_TEXCOORD);

        // first generate points and texture coordinates
        final int n = grid.length;
        final float invTexCoordX = 1.0f / worldSize.x;
        final float invTexCoordY = 1.0f / worldSize.y;
        float[] points = new float[n*3];
        float[] coords = new float[n*2];
        for (int i = 0; i < n; ++i)
        {
            // X-right  Y-down  Z-forward
            Vector3 v = grid[i];
            points[i*3    ] = v.x; // convert from LHS to JavaFX crazy system
            points[i*3 + 1] = -v.z;
            points[i*3 + 2] = v.y;
            coords[i*2    ] = v.x*invTexCoordX;
            coords[i*2 + 1] = 1.0f - v.y*invTexCoordY;
        }

        // now generate triangle indices
        int   numTris = (w-1) *(h-1) * 2;
        int[] indices = new int[numTris*6]; // POINT_TEXCOORD tri has 6 interleaved indices
        int   tindex  = 0;
        for (int y = 0; y < h-1; ++y)
        {
            for (int x = 0; x < w-1; ++x)
            {
                // p0---p1
                // |   /|
                // |  / |
                // | /  |
                // p2---p3
                final int i = (y * w) + x; // idx of p0
                final int j = i + w;       // idx of p2
                indices[tindex    ] = i;   // p0
                indices[tindex + 1] = i;   // t0
                indices[tindex + 2] = i+1; // p1
                indices[tindex + 3] = i+1; // t1
                indices[tindex + 4] = j;   // p2
                indices[tindex + 5] = j;   // t2
                indices[tindex + 6] = i+1; // p1
                indices[tindex + 7] = i+1; // t1
                indices[tindex + 8] = j+1; // p3
                indices[tindex + 9] = j+1; // t3
                indices[tindex +10] = j;   // p2
                indices[tindex +11] = j;   // t2
                tindex += 12;
            }
        }
        mesh.getPoints().setAll(points, 0, points.length);
        mesh.getTexCoords().setAll(coords, 0, coords.length);
        mesh.getFaces().setAll(indices, 0, indices.length);
        return new MeshView3D(mesh);
    }

    /// Arrghh JavaFX3D doesn't have line drawing??
    public void createLineMesh(ArrayList<Vector3> linePairs, float lineWidth)
    {
        TriangleMesh mesh;
        if (getMesh() == null) mesh = new TriangleMesh();
        else mesh = (TriangleMesh)getMesh();

        // Vec3 pos;
        // Vec2 tex;
        mesh.setVertexFormat(VertexFormat.POINT_TEXCOORD);

        // first generate points and texture coordinates
        final int numTris = linePairs.size();
        final int numVertices = numTris * 2;

        float[] points  = new float[numVertices*3];
        float[] coords  = new float[]{ 0.0f, 0.0f }; // dummy coords
        int[]   indices = new int[numTris*6]; // POINT_TEXCOORD tri has 6 interleaved indices
        int vindex = 0;
        int tindex = 0;

        for (int i = 0; i < numTris; i += 2)
        {
            Vector3 a = linePairs.get(i);
            Vector3 b = linePairs.get(i+1);
            Vector3 n = a.rightNormalXY(b).mul(lineWidth);
            Vector3 c = a.add(n);
            Vector3 d = b.add(n);

            points[vindex]     =  a.x; // convert from LHS to JFX coords
            points[vindex + 1] = -a.z;
            points[vindex + 2] =  a.y;
            points[vindex + 3] =  b.x;  // B
            points[vindex + 4] = -b.z;
            points[vindex + 5] =  b.y;
            points[vindex + 6] =  c.x;  // C
            points[vindex + 7] = -c.z;
            points[vindex + 8] =  c.y;
            points[vindex + 9]  =  d.x;  // D
            points[vindex + 10] = -d.z;
            points[vindex + 11] =  d.y;
            vindex += 12;

        }

        // now generate triangle indices
        for (int i = 0; i < numVertices; i += 4)
        {
            // p0---p1
            // |   /|
            // |  / |
            // | /  |
            // p2---p3
            indices[tindex]     = i;   // p0
            indices[tindex + 1] = 0;   // t0
            indices[tindex + 2] = i+1; // p1
            indices[tindex + 3] = 0;   // t1
            indices[tindex + 4] = i+2; // p2
            indices[tindex + 5] = 0;   // t2
            indices[tindex + 6] = i+1; // p1
            indices[tindex + 7] = 0;   // t1
            indices[tindex + 8] = i+3; // p3
            indices[tindex + 9] = 0;   // t3
            indices[tindex +10] = i+2; // p2
            indices[tindex +11] = 0;   // t2
            tindex += 12;
        }
        mesh.getPoints().setAll(points, 0, points.length);
        mesh.getTexCoords().setAll(coords, 0, coords.length);
        mesh.getFaces().setAll(indices, 0, indices.length);
        setMesh(mesh);
    }
}
