/**
 * Copyright (c) 2016 - Jorma Rebane
 * Optimized Astar pathfinder for finding safest shortest path
 * on mountainous topography.
 */
import java.util.*;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.geometry.Point2D;
import javafx.scene.*;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.*;
import javafx.scene.input.*;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.scene.transform.*;
import javafx.stage.Stage;
import javafx.util.Duration;

public class AstarSceneUI extends Application
{
    ///////////////////////////////////////////////////////////////////////////
    ///////////// 3D Scene
    ///////////////////////////////////////////////////////////////////////////

    private double MouseX;
    private double MouseY;
    private Timer FrameTimer      = new Timer(); // calculates deltaTime
    private Timer MouseClickTimer = new Timer(); // need this to differ between mouse drag and mouse click
    private MouseEvent MousePressEvt;
    private double DeltaTime = 0.0;

    // Somewhat important Scene elements
    private Astar     Graph      = new Astar();
    private SubScene  Scene3D;
    private Camera3D  Camera     = new Camera3D();
    private Group3D   CameraNode = new Group3D(Camera); // we use this for cam world rotation and offsets

    private MeshView3D Terrain;
    private MeshView3D TerrainWire;
    private Vector3    TerrainOffset;
    private Vector3    StartPos, EndPos;
    private Box3D      BoxStart, BoxEnd; // A* search start/end markers

    private MeshView3D ExploredView;
    private MeshView3D PathView;
    private Image RedTexture;
    private Image GreenTexture;

    private void onMouseFastPress(MouseEvent me)
    {
        MouseX = me.getSceneX();
        MouseY = me.getSceneY();

        PickResult pick = me.getPickResult();
        if (pick.getIntersectedNode() != Terrain &&
            pick.getIntersectedNode() != TerrainWire &&
            pick.getIntersectedNode() != ExploredView &&
            pick.getIntersectedNode() != PathView)
            return; // only consider Terrain for picks

        System.out.println(pick.toString());

        // the PickResult position is already in local transform
        Vector3 terrainPos = new Vector3(pick.getIntersectedPoint());
        if (me.isPrimaryButtonDown())
        {
            StartPos = terrainPos;
            System.out.printf("StartPos: %s\n", StartPos);
            BoxStart.pos.set(StartPos.add(TerrainOffset));
        }
        else if (me.isSecondaryButtonDown())
        {
            EndPos = terrainPos;
            System.out.printf("EndPos: %s\n", EndPos);
            BoxEnd.pos.set(EndPos.add(TerrainOffset));
        }
        onFindScenePath();
    }

    // segregate path points[A,B,C,D] into a segment list [AB,BC,CD]
    private ArrayList<Vector3> segregatePath(ArrayList<Vector3> points)
    {
        ArrayList<Vector3> segregated = new ArrayList<>();
        segregated.ensureCapacity(points.size() * 2);
        for (int i = 1; i < points.size(); ++i) {
            segregated.add(points.get(i-1));
            segregated.add(points.get(i));
        }
        return segregated;
    }

    private void onFindScenePath()
    {
        if (StartPos == null || EndPos == null)
            return;

        Timer t = new Timer();
        ArrayList<Vector3> explored = new ArrayList<>();
        ArrayList<Vector3> path = Graph.findPath(StartPos, EndPos, explored);
        double e = t.elapsed();

        ExploredView.createLineMesh(explored, 1.0f);
        PathView.createLineMesh(segregatePath(path), 2.0f);
        updateStats(e, path, explored);
    }

    private MeshView3D createLineView(Color color, Image texture, Vector3 pos)
    {
        MeshView3D view = new MeshView3D();
        view.setDepthTest(DepthTest.ENABLE);
        view.setDrawMode(DrawMode.FILL);
        view.setCullFace(CullFace.NONE);
        view.setMaterial(new PhongMaterial(color, texture, null, null, texture));
        view.pos.set(pos);
        return view;
    }

    private Group createContent() throws Exception
    {
        Graph.loadMap();

        TerrainOffset = Graph.WorldSize.div(2).negate();

        Terrain = Graph.createTerrainMesh();
        Terrain.setDrawMode(DrawMode.FILL);
        Terrain.setMaterial(new PhongMaterial(Color.WHITE, null, null, null, null));
        Terrain.pos.set(TerrainOffset);

        if (true)
        {
            TerrainWire = new MeshView3D(Terrain.getMesh());
            TerrainWire.setDrawMode(DrawMode.LINE);
            TerrainWire.setMaterial(new PhongMaterial(Color.DARKGRAY));
            TerrainWire.pos.set(TerrainOffset);
            TerrainWire.setPickOnBounds(false); // disable pick events
        }

        GreenTexture = new Image("http://enos.itcollege.ee/~jrebane/algo/green.bmp");
        RedTexture   = new Image("http://enos.itcollege.ee/~jrebane/algo/red.bmp");
        ExploredView = createLineView(Color.GREEN, GreenTexture, TerrainOffset.add(0,0,1.0f));
        PathView     = createLineView(Color.RED,   RedTexture,   TerrainOffset.add(0,0,1.5f));
        ExploredView.setPickOnBounds(false); // disable pick events
        PathView.setPickOnBounds(false);

        BoxStart = new Box3D(5, 5, 5);
        BoxStart.setMaterial(new PhongMaterial(Color.RED));
        BoxStart.pos.set(TerrainOffset);
        BoxEnd = new Box3D(5, 5, 5);
        BoxEnd.setMaterial(new PhongMaterial(Color.BLUE));
        BoxEnd.pos.set(TerrainOffset.add(0,100,0));

        // Create and position camera
        Camera.setFieldOfView(55);
        Camera.getTransforms().setAll(new Rotate(-20, Rotate.X_AXIS));
        Camera.setFarClip(10000.0);
        Camera.pos.set(0, -500, +200);

        Light3D light = new Light3D(Color.BEIGE);
        light.pos.set(-1000, -1000, +1000);

        // Build the Scene Graph
        Group root = new Group(CameraNode, BoxStart, BoxEnd, light, Terrain, ExploredView, PathView);
        if (TerrainWire != null) root.getChildren().add(TerrainWire);

        Scene3D = new SubScene(root, 1280, 720, true, SceneAntialiasing.BALANCED);
        Scene3D.setFill(Color.ALICEBLUE);
        Scene3D.setCamera(Camera);

        Group group = new Group(Scene3D);
        group.setOnMousePressed((MouseEvent me) -> {
            MouseClickTimer.start();
            MousePressEvt = me;
            MouseX = me.getSceneX();
            MouseY = me.getSceneY();
        });
        group.setOnMouseReleased((MouseEvent me) -> {
            double elapsed = MouseClickTimer.elapsed();
            if (elapsed < 0.2) onMouseFastPress(MousePressEvt);
        });
        group.setOnMouseDragged((MouseEvent me) -> {
            if (me.isPrimaryButtonDown()) {
                double rotx = (me.getSceneY() - MouseY) * DeltaTime * -5;
                double roty = (me.getSceneX() - MouseX) * DeltaTime * +5;
                CameraNode.getTransforms().addAll(
                        new Rotate(rotx, Rotate.X_AXIS),
                        new Rotate(roty, Rotate.Y_AXIS));
            }
            MouseX = me.getSceneX();
            MouseY = me.getSceneY();
        });
        group.setOnScroll((ScrollEvent se) -> {
            double deltaZoom = se.getDeltaY() * DeltaTime * 15;
            CameraNode.getTransforms().add(new Translate(0,0,deltaZoom));
        });
        return group;
    }

    private void frameUpdateEvent(ActionEvent event)
    {
        DeltaTime = FrameTimer.next();
        if (isKeyDown(KeyCode.ESCAPE)) Platform.exit();

        Vector3 move = new Vector3();
        if (isKeyDown(KeyCode.W)) move.y += DeltaTime * 200;
        if (isKeyDown(KeyCode.S)) move.y -= DeltaTime * 200;
        if (isKeyDown(KeyCode.A)) move.x -= DeltaTime * 200;
        if (isKeyDown(KeyCode.D)) move.x += DeltaTime * 200;
        if (!move.isZero()) {
            //Vector3 fwd = Camera.pos.forwardVector();
            //System.out.printf("CameraNode.forwardVector = %s\n", Camera.pos.forwardVector());
            CameraNode.pos.add(move);
        }

        Vector3 rot = new Vector3();
        if (isKeyDown(KeyCode.UP))    rot.x -= DeltaTime * 50;
        if (isKeyDown(KeyCode.DOWN))  rot.x += DeltaTime * 50;
        if (isKeyDown(KeyCode.LEFT))  rot.y -= DeltaTime * 50;
        if (isKeyDown(KeyCode.RIGHT)) rot.y += DeltaTime * 50;
        if (isKeyDown(KeyCode.Q))     rot.z -= DeltaTime * 50;
        if (isKeyDown(KeyCode.E))     rot.z += DeltaTime * 50;
        if (rot.x != 0.0f) CameraNode.getTransforms().add(new Rotate(rot.x, Rotate.X_AXIS));
        if (rot.y != 0.0f) CameraNode.getTransforms().add(new Rotate(rot.y, Rotate.Y_AXIS));
        if (rot.z != 0.0f) CameraNode.getTransforms().add(new Rotate(rot.z, Rotate.Z_AXIS));

        if (!rot.isZero()) {
            System.out.printf("CameraNode.forwardVector = %s\n", CameraNode.pos.forwardVector());
        }
        Scene3D.requestFocus();
    }

    private void setup3DScene(Scene scene)
    {
        scene.setOnKeyReleased((KeyEvent ke)-> Keys.remove(ke.getCode()));
        scene.setOnKeyPressed((KeyEvent ke) -> { if (!isKeyDown(ke.getCode())) Keys.add(ke.getCode());});

        // set up a 30FPS frame loop
        Timeline frameLoop = new Timeline(new KeyFrame(Duration.millis(33.3), this::frameUpdateEvent));
        frameLoop.setCycleCount(Animation.INDEFINITE);
        frameLoop.play();
    }

    @Override public void start(Stage primaryStage) throws Exception
    {
        Group content = createContent();
        Scene scene = new Scene(content);
        setup3DScene(scene);
        setup2DScene(content);
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();
    }






    ////////////////////////////////////////////////////////////////////////////
    ///////////////  2D Scene
    ////////////////////////////////////////////////////////////////////////////
    private Label TextStats = new Label();
    private Slider DModeSlider;
    private Slider HModeSlider;
    private Slider MultiplySlider;
    private Slider ExponentSlider;
    private Label TextDMode = new Label();
    private Label TextHMode = new Label();
    private Label TextMultiply = new Label();
    private Label TextExponent = new Label();

    private Slider createSlider(double min, double max, int majorTick, int minorTick, Point2D pos)
    {
        Slider slider = new Slider(min, max, min);
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
        slider.setMajorTickUnit(majorTick);
        slider.setMinorTickCount(minorTick);
        slider.setSnapToTicks(true);
        slider.setMinWidth(380);
        slider.setStyle("-fx-text-background-color: red;");
        slider.setFocusTraversable(false);
        slider.getTransforms().add(new Translate(pos.getX(), pos.getY()));
        return slider;
    }

    private void updateSliderVisibility()
    {
        boolean mulVisible = Graph.heightMethod() == HeightMethod.Multiply;
        boolean expVisible = Graph.heightMethod() == HeightMethod.Exponential;
        MultiplySlider.setVisible(mulVisible);
        TextMultiply.setVisible(mulVisible);
        ExponentSlider.setVisible(expVisible);
        TextExponent.setVisible(expVisible);
    }

    private void setup2DScene(Group content)
    {
        TextStats.getTransforms().add(new Translate(10,10,0));
        TextStats.setStyle("-fx-font-family: monospace; -fx-text-fill: red; -fx-font-size: 20;");
        content.getChildren().add(TextStats);

        TextDMode.getTransforms().add(new Translate(900, 20));
        TextHMode.getTransforms().add(new Translate(900, 80));
        TextMultiply.getTransforms().add(new Translate(900, 140));
        TextExponent.getTransforms().add(new Translate(900, 140));
        TextDMode.setStyle("-fx-font-family: monospace; -fx-text-fill: red; -fx-font-size: 18;");
        TextHMode.setStyle("-fx-font-family: monospace; -fx-text-fill: red; -fx-font-size: 18;");
        TextMultiply.setStyle("-fx-font-family: monospace; -fx-text-fill: red; -fx-font-size: 18;");
        TextExponent.setStyle("-fx-font-family: monospace; -fx-text-fill: red; -fx-font-size: 18;");
        content.getChildren().addAll(TextDMode, TextHMode, TextMultiply, TextExponent);

        DModeSlider = createSlider(0.0, 2.0, 1, 0, new Point2D(490, 20));
        DModeSlider.setMinWidth(150);
        DModeSlider.valueProperty().addListener((ObservableValue<? extends Number> ov, Number oldVal, Number newVal)-> {
            if (Graph.distanceMethod(DistanceMethod.values()[newVal.intValue()])) onFindScenePath();
            updateSliderVisibility();
            TextDMode.setText(String.format(Locale.US, "Distance: %s", Graph.distanceMethod()));
        });
        HModeSlider = createSlider(0.0, 2.0, 1, 0, new Point2D(490, 80));
        HModeSlider.setMinWidth(150);
        HModeSlider.valueProperty().addListener((ObservableValue<? extends Number> ov, Number oldVal, Number newVal)-> {
            if (Graph.heightMethod(HeightMethod.values()[newVal.intValue()])) onFindScenePath();
            updateSliderVisibility();
            TextHMode.setText(String.format(Locale.US, "Height: %s", Graph.heightMethod()));
        });
        MultiplySlider = createSlider(0.0, Astar.MaxHeightMultiplier, 10, 5, new Point2D(490, 140));
        MultiplySlider.valueProperty().addListener((ObservableValue<? extends Number> ov, Number oldVal, Number newVal)-> {
            if (Graph.heightMultiplier(newVal.floatValue())) onFindScenePath();
            TextMultiply.setText(String.format(Locale.US, "H.Multiplier: %.1f", Graph.heightMultiplier()));
        });
        ExponentSlider = createSlider(0.1, Astar.MaxHeightExponent, 1, 2, new Point2D(490, 140));
        ExponentSlider.valueProperty().addListener((ObservableValue<? extends Number> ov, Number oldVal, Number newVal)-> {
            if (Graph.heightExponent(newVal.floatValue())) onFindScenePath();
            TextExponent.setText(String.format(Locale.US, "H.Exponent:   %.1f", Graph.heightExponent()));
            content.requestFocus();
        });

        DModeSlider.setValue(Graph.distanceMethod().ordinal());
        HModeSlider.setValue(Graph.heightMethod().ordinal());
        MultiplySlider.setValue(Graph.heightMultiplier());
        ExponentSlider.setValue(Graph.heightExponent());

        content.getChildren().addAll(DModeSlider, HModeSlider, MultiplySlider, ExponentSlider);
    }

    private void updateStats(double elapsed, ArrayList<Vector3> path, ArrayList<Vector3> explored)
    {
        String text = String.format("Elapsed:      %.3fms\n"+
                                    "MaxDepth:     %d\n"+
                                    "NumOpened:    %d\n"+
                                    "NumReopened:  %d\n"+
                                    "NumExplored:  %d\n"+
                                    "PathLength:   %d\n"+
                                    "AvgInsShift:  %3.1f\n"+
                                    "AvgReposRev:  %3.1f\n"+
                                    "AvgReposFwd:  %3.1f\n",
                                        elapsed * 1000, Graph.MaxDepth, Graph.NumOpened,
                                        Graph.NumReopened, explored.size(), path.size(),
                                        AstarNodeVector.AvgInsertShift, AstarNodeVector.AvgReposRev, AstarNodeVector.AvgReposFwd);
        System.out.print(text);
        System.out.printf("Path: %s\n", path.toString());
        TextStats.setText(text);
    }

    public static void main(String[] args)
    {
        AstarSceneUI.launch(args);
    }

    // Key utils
    private ArrayList<KeyCode> Keys = new ArrayList<>();
    private boolean isKeyDown(KeyCode kc) { return Keys.indexOf(kc) != -1; }

}

