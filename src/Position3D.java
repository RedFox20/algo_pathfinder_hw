import javafx.geometry.Point3D;
import javafx.scene.Node;
import javafx.scene.transform.Transform;

/**
 * Position3D strategy pattern extension for JFX to 'get good' :)
 */
public class Position3D
{
    private Node o; // owner
    public Position3D(Node o) { this.o = o; }

    Position3D set(float x, float y, float z) {
        o.setTranslateX(x); /** @note LHS to JFX conv */
        o.setTranslateY(-z);
        o.setTranslateZ(y);
        return this;
    }
    public final Position3D add(float x, float y, float z) {
        o.setTranslateX(o.getTranslateX() + x);
        o.setTranslateY(o.getTranslateY() - z);
        o.setTranslateZ(o.getTranslateZ() + y);
        return this;
    }
    public final Position3D set(Vector3 v) { return set(v.x, v.y, v.z); }
    public final Position3D add(Vector3 v) { return add(v.x, v.y, v.z); }

    public final Vector3 get() {
        return new Vector3( /** @note JFX to LHS conv */
                (float) o.getTranslateX(),
                (float) o.getTranslateZ(),
                -(float) o.getTranslateY());
    }

    public final boolean isZero() { return get().isZero(); }

    /** @brief Returns the current forward direction of the actor -- depends on rotation */
    public final Vector3 forwardVector() {
        Transform t = o.getLocalToSceneTransform();
        return new Vector3( /** @note JFX to LHS conv */
                -(float)t.getMzy(),  // right
                (float)t.getMzz(),  // forward
                (float)t.getMzx()); // up
    }
}
