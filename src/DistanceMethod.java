
/**
 * @brief Distance method controls how distance between 3D and 2D points is calculated
 */
public enum DistanceMethod
{
    ManhattanDistance, // manhattan distance is the inner sum of grid deltaDistance: X+Y+Z
    SquaredDistance,   // squared distance gives approximate results
    Accurate,          // distance method
}
