 /**
  * @brief Custom tailored pathfinding vector optimized specifically for Astar
  * Items are sorted in descending order by FScore.
  * Item repositioning, which is very frequent in Astar, is optimized to
  */
public class AstarNodeVector
{
    private AstarNode[] Data = null;
    private int Size     = 0;
    private int Capacity = 0;

    // some statistics... to get better idea of what to optimize
    public static float AvgInsertShift = 0.0f; // average number of elements shifted during insert
    public static float AvgReposRev    = 0.0f; // average number of elements shifted reverse during repos
    public static float AvgReposFwd    = 0.0f; // average number of elements shifted forward during repos

    public AstarNodeVector() {}
    public AstarNodeVector(int cap) { reserve(cap); }
    public void      clear()        { Size = 0; }
    public int       size()         { return Size; }
    public boolean   empty()        { return Size == 0;   }
    public AstarNode pop()          { return Data[--Size];}
    public AstarNode get(int index) { return Data[index]; }

    public void reserve(int capacity)
    {
        AstarNode[] data = new AstarNode[Capacity = capacity];
        if (Data != null) System.arraycopy(Data, 0, data, 0, Size);
        Data = data;
    }

    /**
     * @brief Performs a binary insertion
     * @param item New item to insert
     */
    public void insert(AstarNode item)
    {
        int imax = Size;
        int imin = 0;
        assert imax < Capacity;
        final float itemScore = item.FScore;

        // binary search for appropriate index
        while (imin < imax)
        {
            int imid = (imin + imax) / 2;
            if (Data[imid].FScore > itemScore)
                imin = ++imid;
            else
                imax = imid;
        }

        AvgInsertShift = (AvgInsertShift + (Size - imin)) * 0.5f;

        // shift everything else forward once:
        System.arraycopy(Data, imin, Data, imin+1, Size);
        Data[imin] = item;
        ++Size;
    }

    /**
     * @brief instead of erase/insert, we reposition the item because
     *        we know Astar most often repositions items at the end of openlist
     * @param item Existing item to reposition/update
     */
    public void repos(AstarNode item)
    {
        for (int i = Size-1; i >= 0; --i) // reverse iter
        {
            if (Data[i] != item) // ref match is enough
                continue;

            final int istart = i;
            final float itemScore = item.FScore;
            if (i != 0) // backwards movement
            {
                final float prevScore = Data[i-1].FScore;
                if (prevScore == itemScore) return; // it's sorted
                if (prevScore < itemScore) // we have to move backwards
                {
                    do // shift backward until sorted
                    {
                        AstarNode prev = Data[--i];
                        if (prev.FScore >= itemScore) break;
                        Data[i]   = item;
                        Data[i+1] = prev;
                    } while (i > 0);
                    AvgReposRev = (AvgReposRev + (istart-i)) * 0.5f;
                    return;
                }
            }
            final int last = Size-1;
            if (i < last) // forward shift
            {
                do // shift forward until sorted
                {
                    AstarNode next = Data[++i];
                    if (itemScore >= next.FScore) break; // it's sorted
                    Data[i]   = item;
                    Data[i-1] = next;
                } while (i < last);
            }
            AvgReposFwd = (AvgReposFwd + (i-istart)) * 0.5f;
            return; // done. openlist is sorted
        }
    }
}
